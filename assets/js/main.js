jQuery.fn.outerHTML = function(s) {return s ? this.before(s).remove() : jQuery("<p>").append(this.eq(0).clone()).html();};

mapInit = function(){
    $('.addressListBlock').each(function(e){
		var owner = this;
		var placemarks = [];
		
		$('.addressList [data-geo-coord]',this).each(function(i,el){
			var coord = this.getAttribute('data-geo-coord');
			if (!coord) {return false}
			coord = coord.split(',');
			
			
			placemarks.push(new ymaps.Placemark(coord, {
				iconContent : $(owner).hasClass('point-pvz')?'':i+1,
			}, {
                /*preset: 'islands#nightCircleIcon',*/
                iconLayout: 'default#image',
                iconImageHref: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNjIuMDYiIHdpZHRoPSI0NSI+PHBhdGggZmlsbD0iI2EyMzI2OCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNMjIuNSA2Mi4wNlM0NSA0NC4zMyA0NSAyMi4xN2EyMi41IDIyLjUgMCAwIDAtNDUgMGMwIDIyLjE2IDIyLjUgMzkuOSAyMi41IDM5Ljl6bS05LTM5Ljg5YTkgOSAwIDEgMSA5IDguODcgOC45NCA4Ljk0IDAgMCAxLTktOC44N3oiIGRhdGEtbmFtZT0iQ29tcG9zaXRlIFBhdGgiLz48L3N2Zz4=',
                iconImageSize: [45, 62],
                iconImageOffset: [-22.5, -62],
				coords: coord
			}));
		});
		
		
		if (!placemarks.length)
		{
			return false;
		}
        if (placemarks.length>1)  {
            var map = new ymaps.Map('map', {center:[61.698653,99.505405],zoom:10,controls: ['zoomControl', 'fullscreenControl']}, {autoFitToViewport: 'always'});
            for(var i=0;i<placemarks.length;i++)
            {
                map.geoObjects.add(placemarks[i]);
            }
        } else {

            
            var coord = $('.addressList [data-geo-coord]').attr('data-geo-coord');
			if (!coord) {return false}
            coord = coord.split(',');
            var zoom = $('.addressList [data-geo-coord]').attr('data-zoom');
            if (!coord) {zoom = 10};
            var map = new ymaps.Map('map', {center:coord ,zoom:zoom,controls: ['zoomControl', 'fullscreenControl']}, {autoFitToViewport: 'always'});
            map.geoObjects.add(placemarks[0]);
        }
    });
}


var __opts = {popup:{}}
__opts.popup.defaults = {
		tClose: 'Закрыть (Esc)',
		tLoading: 'Загрузка...',
		type: 'inline',
		fixedContentPos: true,
		fixedBgPos: true,
		image: {
			tError: 'Произошла ошибка при загрузке <a href="%url%">изображения</a>.'
		},
		ajax: {
			settings: null,
			cursor: 'isProcessed',
			tError: 'Ошибка при загрузке <a href="%url%">содержимого</a>'
		},
		callbacks: {
			beforeOpen: function() {
				$('body').addClass('withModalOpen');
			},
			open: function() {
				$('table').not('.tableWrapper table').wrap('<div class="tableWrapper"></div>');
				$('.field :input').blur();

      },
			change: function() {
				$('table').not('.tableWrapper table').wrap('<div class="tableWrapper"></div>');
				$('.field :input').blur();

      },
			ajaxContentAdded: function() {

      },
			afterClose: function() {
				$('body').removeClass('withModalOpen');
			},
    },

		verticalFit: true,

		closeBtnInside: true,
		preloader: true,
		removalDelay: 500,
		mainClass:'mfp-move-horizontal'
	};
__opts.popup.gallery = $.extend(true,{},__opts.popup.defaults,{
		gallery: {
			enabled: true,
			tPrev: 'Предыдущее (&larr;)',
			tNext: 'Следующее (&rarr;)',
			tCounter: '%curr% из %total%',
			preload: [1,2],
		},
		modal:false,
		type: 'image',
		closeBtnInside: false,
		mainClass:'mfp-move-horizontal mfp-mode-gallery'
	})


function csrfSafeMethod(method) {
	// these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
		cache: false,
    beforeSend: function(xhr, settings) {
			var csrftoken = $('[name="csrfmiddlewaretoken"]').val();
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
					xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
    }
});


var ajaxRequest = function(e){
	var url, enctype, method;
	var counter = false;
	var formAjax = false;
	if (e.type=='submit')
	{
		url = this.action;
		enctype = this.enctype;
		method = this.method;
	}

	if (e.type=='click')
	{
		url = this.getAttribute('href');
		enctype = false;
		method = 'get';
	}
	if ($(this).closest('.counterBlock').length) {
		counter = true;
	}
	if($(this).is('[data-transfer-ajax]')) {
		var formAjax = true;
		var inputsHidden = '';
		$('[data-transfer-ajax]',this).each(function() {
			var valueInput = $(this).val(),
				name = $(this).data("transfer-ajax");
				inputsHidden+='<input type="hidden" name="'+name+'" value="'+valueInput+'">';
		});
	}
	var src = ($(this).data('request-source') || ('#'+url.split('#')[1])).trim();
	var trg = $(this).data('request-target');
	var url = url.split('#')[0];

	if (!src) {return}

	e.preventDefault();

	var owner = this;

	var data;

	method = (method||'get').toUpperCase();

	if (e.type=='submit')
	{
		if (method=='POST')
		{
			data = new FormData(this);
		}
		else
		{
			data = $(this).serializeArray();
		}
	}

	$(owner).addClass('isProcessing');
	$(trg=='@self'?owner:src).addClass('isWaiting');

	if(url)
	{

		$.ajax({url:url||window.location.href, processData:method=='POST'?false:true, contentType:false, cache:false, type:method, method:method, data:data}).done(function(data){

			$(owner).removeClass('isProcessing');

			var html = $('<div>'+data+'</div>').find(src);
			if (!html.length)
			{
				html = "<div class='message message-error'>Ошибка при загрузке</div>"
			}

			switch (trg)
			{
				case '@modal':
				case '@popup':
				{
					var type = trg.split('@')[1];
					var param = $(owner).data('request-param');
					param = param ? param.split(';') : [];
					for (var i=0;i<param.length;i++)
					{
						param[i]=param[i].split(':');
						param[i]="data-param-"+param[i][0]+(param[i][1]?("='"+param[i][1]+"'"):'');
					}
					var options = {items: {src:$("<div class='popupBlock popupBlock-"+type+"' data-request-source='"+src+"' data-request-url='"+url+"' "+($(owner).data('request-title')?('data-request-title="'+$(owner).data('request-title')+'"'):'')+(param.length?param.join(' '):'')+"><div class='blockWrapper'>"+($(owner).data('request-title')?("<h2 class='blockTitle'>"+$(owner).data('request-title')+"</h2>"):'')+"<div class='blockContent'>"+html.outerHTML()+"</div></div></div>")},type:'inline'};
					var magnificPopup = $.magnificPopup.instance;
					$.magnificPopup.open($.extend(true,{},__opts.popup.defaults,options));

					$('.mfp-content').attr('data-counter',counter);
					if (formAjax) {
						$('.mfp-content form').prepend(inputsHidden);
					}
					break;
				}
				case '@blank':
				{
					var w = window.open();
					w.document.write(html.html());
					w.document.close();
					break;
				}
				default:
				{
					$(trg=='@self'?owner:src)[$(this).data('request-insert')?($(this).data('request-insert')=='prepend'?'prepend':'append'):'html'](html.html()).removeClass('isWaiting');

					/* update content behavior */
					$('table').not('.tableHolder table').wrap('<div class="tableHolder"></div>');
					$('.field :input').blur();
				}
			}

		});
	}
	else
	{
		var type = trg.split('@')[1];
		var param = $(owner).data('request-param');
		param = param ? param.split(';') : [];
		for (var i=0;i<param.length;i++)
		{
			param[i]=param[i].split(':');
			param[i]="data-param-"+param[i][0]+(param[i][1]?("='"+param[i][1]+"'"):'');
		}
		var options = {items: {src:$("<div class='popupBlock popupBlock-"+type+"' data-request-source='"+src+"' data-request-url='"+url+"' "+(param.length?param.join(' '):'')+"><div class='blockWrapper'>"+($(owner).data('request-title')?("<h2 class='blockTitle'>"+$(owner).data('request-title')+"</h2>"):'')+"<div class='blockContent'>"+$(src).html()+"</div></div></div>")},type:'inline'};
		$.magnificPopup.open($.extend(true,{},__opts.popup.defaults,options));
	}

	return false;
}


var morph = {
	count: function(number, titles)
	{
		if (!titles[2]) {titles[2]=titles[1];}
		cases = [2, 0, 1, 1, 1, 2];
		return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
	}
}


















$(function() {
    if ($.fn.mask)
	{
		$('body').on('focus','input.phone,input[type="tel"],.field-phone input',function(){
			$(this).mask('+7 (999) 999-99-99');
		});
		$('body').on('focus','[data-mask]',function(){
			$(this).mask($(this).data('mask'));
		});
	}

	$(window).scroll(function() {
		if ($(this).scrollTop() > 120 ) {
			$('body').addClass('scrollMenu');
		} else {$('body').removeClass('scrollMenu');}
	});

	$('table').not('.tableWrapper table').wrap('<div class="tableWrapper"></div>');
	
	$('body').on('submit','form[data-request-source]',ajaxRequest);
	$('body').on('click','a[data-request-source]',ajaxRequest);

    $('body').on('click','a.hrefid',function(e){

        var hash = this.href.split('#')[1];

        var el = hash?$('#'+hash):false;

        if (hash && el && el.length)

        {

            $('html,body').stop().animate({scrollTop:el.offset().top});

            $('body').removeClass('menuSwitched');

        }

    })

    $('.inputNumberBlock').each(function() {
        var owner = this;
        $('.step',owner).click(function() {
            if ($(this).hasClass('stepLess')) {
                var i = Number($('input[type="number"]',owner).val());
                if(i<=0) {
                    $('input[type="number"]',owner).val(0);
                }
                else {
                    $('input[type="number"]',owner).val(i-1);
                }
                
            } else {
                $('input[type="number"]',owner).val(Number($('input[type="number"]',owner).val())+1)
            }
            return false;
        });
    });

    $('.tabsBlock').each(function() {
        var owner = this;

        $('.tabsTitleBlock',owner).on('click','.tabsTitle',function() {
            $('.tabsTitleBlock .tabsTitle',owner).removeClass('active');
            $('.tabsContentBlock .tabsContent',owner).removeClass('active');
            $($(this).attr("href")).addClass('active');
            $(this).addClass('active');
            return false;
        });
        $('.tabsTitleBlock .tabsTitle:first-child').click();
    });
    $('.menu').click(function() {
		$(this).parent().toggleClass('menuOpen');
		$('body').toggleClass('isMenuActive');
	});
	
	/*time */

	$('[data-stop]').each(function(){
		var owner = this;

		var lang = {
			ru:{d:['день','дня','дней'],h:'час',m:'мин',s:'сек'},
			en:{d:['day','days'],h:'hrs',m:'min',s:'sec'},
			zh:{d:['day','days'],h:'hrs',m:'min',s:'sec'},
		}

		$('>.blockWrapper',this).append($("<div class='timerBlock'><span class='counter'><span class='seg seg-days'><span class='val'></span> <span class='caption'>"+lang[$('html').attr('lang')||'ru'].d[0]+"</span></span><span class='seg seg-hours'><span class='val'></span> <span class='caption'>"+lang[$('html').attr('lang')||'ru'].h+"</span></span><span class='seg seg-minutes'><span class='val'></span> <span class='caption'>"+lang[$('html').attr('lang')||'ru'].m+"</span></span><span class='seg seg-seconds'><span class='val'></span> <span class='caption'>"+lang[$('html').attr('lang')||'ru'].s+"</span></span></span></div>"));

		var stop = $(this).data('stop');
		if (!stop) {return;}


		stop = stop.split(' ');
		if (!stop[1]) {stop[1]='00:00:00';}

		stop[0] = stop[0].split('.');
		stop[1] = stop[1].split(':');
		if (!stop[1][2]) {stop[1][2]=0;}
		console.log(stop);
		var tz1,tz2;
		if ($(this).data('timezone'))
		{
			tz2 = $(this).data('timezone');
			tz1 = new Date();
			tz1 = tz1.toString().split("GMT")[1].split(" (")[0];

			tz1 = {h:tz1.substr(1,2),m:tz1.substr(3,2),dir:tz1.charAt(0)=='-'?-1:1};
			tz2 = {h:tz2.substr(1,2),m:tz2.substr(3,2),dir:tz2.charAt(0)=='-'?-1:1};
		}
		else
		{
			tz1 = {h:0,m:0,dir:0};
			tz2 = {h:0,m:0,dir:0};
		}

		stop = new Date(parseInt(stop[0][2]), parseInt(stop[0][1])-1, parseInt(stop[0][0]), parseInt(stop[1][0])-(tz2.dir*tz2.h)+(tz1.dir*tz1.h)+((tz2.dir*tz2.h)-(tz1.dir*tz1.h)), parseInt(stop[1][1])-(tz2.dir*tz2.m)+(tz1.dir*tz1.m)+((tz2.dir*tz2.m)-(tz1.dir*tz1.m)), parseInt(stop[1][2]));

		this.__recalc = function(){
			var etr = new Date();
			var etr = stop - etr;
			if (etr < 0) {
				etr = 0;
			};

			etr = new Date(etr);

			var secs = (parseInt(etr / 1000));
			var days = (parseInt(secs / (24 * 60 * 60)));
			var secs = secs - days * 24 * 3600;
			var hours = (parseInt(secs / 3600));
			var secs = secs - hours * 3600;
			var mins = (parseInt(secs / 60));
			var secs = secs - mins * 60;
			function setTime(e) {
				if (String(e).length <=1) {
					e = "<span>"+0+"</span>"+"<span>"+String(e)[0]+"</span>";
				} else {
					e = "<span>"+String(e)[0]+"</span>"+"<span>"+String(e)[1]+"</span>";
				}
				return e;
			}
			days = setTime(days);//"<span>"+String(days)[0]+"</span>"+"<span>"+String(days)[1]+"</span>";
			hours = setTime(hours);//"<span>"+String(hours)[0]+"</span>"+"<span>"+String(hours)[1]+"</span>";
			mins = setTime(mins);//"<span>"+String(mins)[0]+"</span>"+"<span>"+String(mins)[1]+"</span>";
			secs = setTime(secs);//"<span>"+String(mins)[0]+"</span>"+"<span>"+String(mins)[1]+"</span>";
			$('.timerBlock .seg-days .caption',owner).html(morph.count(days,lang[$('html').attr('lang')||'ru'].d));
			$('.timerBlock .seg-days .val',owner).html(days);
			$('.timerBlock .seg-hours .val',owner).html(hours);
			$('.timerBlock .seg-minutes .val',owner).html((mins<10?'0':'')+mins);
			$('.timerBlock .seg-seconds .val',owner).html((secs<10?'0':'')+secs);
		}

		this.__recalc();
		this.__interval = window.setInterval(this.__recalc,1000);
	})
});




if ('objectFit' in document.documentElement.style === false) {
	var objectFitImages=function(){"use strict";function t(t,e){return"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='"+t+"' height='"+e+"'%3E%3C/svg%3E"}function e(t){if(t.srcset&&!m&&window.picturefill){var e=window.picturefill._;t[e.ns]&&t[e.ns].evaled||e.fillImg(t,{reselect:!0}),t[e.ns].curSrc||(t[e.ns].supported=!1,e.fillImg(t,{reselect:!0})),t.currentSrc=t[e.ns].curSrc||t.src}}function i(t){for(var e,i=getComputedStyle(t).fontFamily,r={};null!==(e=l.exec(i));)r[e[1]]=e[2];return r}function r(e,i,r){var n=t(i||1,r||0);p.call(e,"src")!==n&&b.call(e,"src",n)}function n(t,e){t.naturalWidth?e(t):setTimeout(n,100,t,e)}function c(t){var c=i(t),o=t[a];if(c["object-fit"]=c["object-fit"]||"fill",!o.img){if("fill"===c["object-fit"])return;if(!o.skipTest&&g&&!c["object-position"])return}if(!o.img){o.img=new Image(t.width,t.height),o.img.srcset=p.call(t,"data-ofi-srcset")||t.srcset,o.img.src=p.call(t,"data-ofi-src")||t.src,b.call(t,"data-ofi-src",t.src),t.srcset&&b.call(t,"data-ofi-srcset",t.srcset),r(t,t.naturalWidth||t.width,t.naturalHeight||t.height),t.srcset&&(t.srcset="");try{s(t)}catch(t){window.console&&console.warn("https://bit.ly/ofi-old-browser")}}e(o.img),t.style.backgroundImage='url("'+(o.img.currentSrc||o.img.src).replace(/"/g,'\\"')+'")',t.style.backgroundPosition=c["object-position"]||"center",t.style.backgroundRepeat="no-repeat",t.style.backgroundOrigin="content-box",/scale-down/.test(c["object-fit"])?n(o.img,function(){o.img.naturalWidth>t.width||o.img.naturalHeight>t.height?t.style.backgroundSize="contain":t.style.backgroundSize="auto"}):t.style.backgroundSize=c["object-fit"].replace("none","auto").replace("fill","100% 100%"),n(o.img,function(e){r(t,e.naturalWidth,e.naturalHeight)})}function s(t){var e={get:function(e){return t[a].img[e||"src"]},set:function(e,i){return t[a].img[i||"src"]=e,b.call(t,"data-ofi-"+i,e),c(t),e}};Object.defineProperty(t,"src",e),Object.defineProperty(t,"currentSrc",{get:function(){return e.get("currentSrc")}}),Object.defineProperty(t,"srcset",{get:function(){return e.get("srcset")},set:function(t){return e.set(t,"srcset")}})}function o(t,e){var i=!h&&!t;if(e=e||{},t=t||"img",f&&!e.skipTest||!d)return!1;"img"===t?t=document.getElementsByTagName("img"):"string"==typeof t?t=document.querySelectorAll(t):"length"in t||(t=[t]);for(var r=0;r<t.length;r++)t[r][a]=t[r][a]||{skipTest:e.skipTest},c(t[r]);i&&(document.body.addEventListener("load",function(t){"IMG"===t.target.tagName&&o(t.target,{skipTest:e.skipTest})},!0),h=!0,t="img"),e.watchMQ&&window.addEventListener("resize",o.bind(null,t,{skipTest:e.skipTest}))}var a="bfred-it:object-fit-images",l=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g,u="undefined"==typeof Image?{style:{"object-position":1}}:new Image,g="object-fit"in u.style,f="object-position"in u.style,d="background-size"in u.style,m="string"==typeof u.currentSrc,p=u.getAttribute,b=u.setAttribute,h=!1;return o.supportsObjectFit=g,o.supportsObjectPosition=f,function(){function t(t,e){return t[a]&&t[a].img&&("src"===e||"srcset"===e)?t[a].img:t}f||(HTMLImageElement.prototype.getAttribute=function(e){return p.call(t(this,e),e)},HTMLImageElement.prototype.setAttribute=function(e,i){return b.call(t(this,e),e,String(i))})}(),o}();
	objectFitImages();
} 